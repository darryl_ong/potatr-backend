package com.potatr.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class ContactPermissions {
	@Id private Long id;
	@Index private String userId;
	@Index private String contactId;
	
	private String seeEmail;
	private String seeFirstName;
	private String seeLastName;
	private String seePosition;
	private String seeDepartment;
	private String seeMobileNumber;
	private String seeOfficeNumber;
	private String seeOfficeAddress;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getSeeEmail() {
		return seeEmail;
	}
	public void setSeeEmail(String seeEmail) {
		this.seeEmail = seeEmail;
	}
	public String getSeeFirstName() {
		return seeFirstName;
	}
	public void setSeeFirstName(String seeFirstName) {
		this.seeFirstName = seeFirstName;
	}
	public String getSeeLastName() {
		return seeLastName;
	}
	public void setSeeLastName(String seeLastName) {
		this.seeLastName = seeLastName;
	}
	public String getSeePosition() {
		return seePosition;
	}
	public void setSeePosition(String seePosition) {
		this.seePosition = seePosition;
	}
	public String getSeeDepartment() {
		return seeDepartment;
	}
	public void setSeeDepartment(String seeDepartment) {
		this.seeDepartment = seeDepartment;
	}
	public String getSeeMobileNumber() {
		return seeMobileNumber;
	}
	public void setSeeMobileNumber(String seeMobileNumber) {
		this.seeMobileNumber = seeMobileNumber;
	}
	public String getSeeOfficeNumber() {
		return seeOfficeNumber;
	}
	public void setSeeOfficeNumber(String seeOfficeNumber) {
		this.seeOfficeNumber = seeOfficeNumber;
	}
	public String getSeeOfficeAddress() {
		return seeOfficeAddress;
	}
	public void setSeeOfficeAddress(String seeOfficeAddress) {
		this.seeOfficeAddress = seeOfficeAddress;
	}
}