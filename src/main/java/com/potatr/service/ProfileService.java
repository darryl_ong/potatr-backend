package com.potatr.service;

import com.potatr.dao.ProfileDAO;
import com.potatr.dto.ProfileDTO;
import com.potatr.model.Profile;

public class ProfileService {
	ProfileDAO pDao = new ProfileDAO();
	
	public ProfileDTO getUserProfile(String userId){
		ProfileDTO pDto = null;
		Profile profile = pDao.getByUserId(userId);

		if(profile != null){
			pDto = new ProfileDTO();

			pDto.setEmail(profile.getEmail());
			pDto.setFirstName(profile.getFirstName());
			pDto.setLastName(profile.getLastName());
			pDto.setPosition(profile.getPosition());
			pDto.setDepartment(profile.getDepartment());
			pDto.setMobileNumber(profile.getMobileNumber());
			pDto.setOfficeAddress(profile.getOfficeAddress());
			pDto.setOfficeNumber(profile.getOfficeNumber());
		}
		
		return pDto;
	}
	
	public void updateUserProfile(String userId, ProfileDTO pDto){
		Profile profile = pDao.getByUserId(userId);
		//create a new Profile for non-existing users
		if(profile == null){
			profile = new Profile();
			profile.setUserId(userId);
		}
		profile.setEmail(pDto.getEmail());
		profile.setFirstName(pDto.getFirstName());
		profile.setLastName(pDto.getLastName());
		profile.setPosition(pDto.getPosition());
		profile.setDepartment(pDto.getDepartment());
		profile.setMobileNumber(pDto.getMobileNumber());
		profile.setOfficeAddress(pDto.getOfficeAddress());
		profile.setOfficeNumber(pDto.getOfficeNumber());
		
		pDao.put(profile);
	}
}
