package com.potatr.service;

import java.util.ArrayList;
import java.util.List;

import com.potatr.constants.ValueConstants;
import com.potatr.dao.ContactPermissionsDAO;
import com.potatr.dao.ProfileDAO;
import com.potatr.dto.ContactPermissionsDTO;
import com.potatr.dto.ProfileDTO;
import com.potatr.model.ContactPermissions;
import com.potatr.model.Profile;

public class ContactService {
	ContactPermissionsDAO cpDao = new ContactPermissionsDAO();
	ProfileDAO pDao = new ProfileDAO();
	
	public List<ProfileDTO> getContacts(String userId){
		List<ProfileDTO> pdList = new ArrayList<ProfileDTO>();
		List<ContactPermissions> cpList = cpDao.listByUserId(userId);
		
		for(ContactPermissions cp : cpList){
			Profile contact = pDao.getByUserId(cp.getContactId());
			
			ProfileDTO pd = new ProfileDTO();
			if(contact != null){
				pd.setUserId(contact.getUserId());
				pd.setEmail(ValueConstants.YES.equals(cp.getSeeEmail()) ? contact.getEmail() : null);
				pd.setFirstName(ValueConstants.YES.equals(cp.getSeeFirstName()) ? contact.getFirstName() : null);
				pd.setLastName(ValueConstants.YES.equals(cp.getSeeLastName()) ? contact.getLastName() : null);
				pd.setPosition(ValueConstants.YES.equals(cp.getSeePosition()) ? contact.getPosition() : null);
				pd.setDepartment(ValueConstants.YES.equals(cp.getSeeDepartment()) ? contact.getDepartment() : null);
				pd.setMobileNumber(ValueConstants.YES.equals(cp.getSeeMobileNumber()) ? contact.getMobileNumber() : null);
				pd.setOfficeAddress(ValueConstants.YES.equals(cp.getSeeOfficeAddress()) ? contact.getOfficeAddress() : null);
				pd.setOfficeNumber(ValueConstants.YES.equals(cp.getSeeOfficeNumber()) ? contact.getOfficeNumber() : null);
			}
			pdList.add(pd);

		}
		
		return pdList;
	}
	
	public void putContact(String userId, ContactPermissionsDTO cpd){
		ContactPermissions cp = new ContactPermissions();
		cp.setUserId(userId);
		cp.setContactId(cpd.getContactId());
		cp.setSeeEmail(cpd.getSeeEmail());
		cp.setSeeFirstName(cpd.getSeeFirstName());
		cp.setSeeLastName(cpd.getSeeLastName());
		cp.setSeePosition(cpd.getSeePosition());
		cp.setSeeDepartment(cpd.getSeeDepartment());
		cp.setSeeMobileNumber(cpd.getSeeMobileNumber());
		cp.setSeeOfficeAddress(cpd.getSeeOfficeAddress());
		cp.setSeeOfficeNumber(cpd.getSeeOfficeNumber());
		
		cpDao.put(cp);
	}
}
