package com.potatr.dao;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.potatr.model.ContactPermissions;

public class ContactPermissionsDAO {
	public List<ContactPermissions> listByUserId(String userId){	
		return ofy().load().type(ContactPermissions.class).filter("userId", userId).list();
	}
	
	public void put(ContactPermissions cp){	
		ofy().save().entity(cp).now();
	}
}
