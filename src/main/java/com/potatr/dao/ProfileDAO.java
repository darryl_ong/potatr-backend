package com.potatr.dao;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.googlecode.objectify.Key;
import com.potatr.model.Profile;

public class ProfileDAO {
	public Profile getByUserId(String userId){	
		return ofy().load().type(Profile.class).filter("userId", userId).first().now();
	}
	
	public void put(Profile profile){	
		ofy().save().entity(profile).now();
	}
}
