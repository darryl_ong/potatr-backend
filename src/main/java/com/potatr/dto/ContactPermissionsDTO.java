package com.potatr.dto;

public class ContactPermissionsDTO {
	private String contactId;
	
	private String seeEmail;
	private String seeFirstName;
	private String seeLastName;
	private String seePosition;
	private String seeDepartment;
	private String seeMobileNumber;
	private String seeOfficeNumber;
	private String seeOfficeAddress;

	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getSeeFirstName() {
		return seeFirstName;
	}
	public void setSeeFirstName(String seeFirstName) {
		this.seeFirstName = seeFirstName;
	}
	public String getSeeLastName() {
		return seeLastName;
	}
	public void setSeeLastName(String seeLastName) {
		this.seeLastName = seeLastName;
	}
	public String getSeePosition() {
		return seePosition;
	}
	public void setSeePosition(String seePosition) {
		this.seePosition = seePosition;
	}
	public String getSeeDepartment() {
		return seeDepartment;
	}
	public void setSeeDepartment(String seeDepartment) {
		this.seeDepartment = seeDepartment;
	}
	public String getSeeMobileNumber() {
		return seeMobileNumber;
	}
	public void setSeeMobileNumber(String seeMobileNumber) {
		this.seeMobileNumber = seeMobileNumber;
	}
	public String getSeeOfficeNumber() {
		return seeOfficeNumber;
	}
	public void setSeeOfficeNumber(String seeOfficeNumber) {
		this.seeOfficeNumber = seeOfficeNumber;
	}
	public String getSeeOfficeAddress() {
		return seeOfficeAddress;
	}
	public void setSeeOfficeAddress(String seeOfficeAddress) {
		this.seeOfficeAddress = seeOfficeAddress;
	}
	public String getSeeEmail() {
		return seeEmail;
	}
	public void setSeeEmail(String seeEmail) {
		this.seeEmail = seeEmail;
	}
}
