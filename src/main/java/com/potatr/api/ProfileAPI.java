package com.potatr.api;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.potatr.constants.AuthConstants;
import com.potatr.dto.ProfileDTO;
import com.potatr.service.ProfileService;

@Api(
    name = "potatr",
    version = "v1",
    scopes = {AuthConstants.EMAIL_SCOPE},
    clientIds = {AuthConstants.WEB_CLIENT_ID, AuthConstants.API_EXPLORER_CLIENT_ID}
)
public class ProfileAPI {
	ProfileService ps = new ProfileService();

	@ApiMethod(name = "profile.get", path="profile/{userId}", httpMethod = HttpMethod.GET)
	public ProfileDTO getProfile(@Named("userId") String userId) throws UnauthorizedException{
		if(userId != null){
			return ps.getUserProfile(userId);
		}else{
			throw new UnauthorizedException("User is not authorized.");
		}
	}
	
	@ApiMethod(name = "profile.put", path="profile/{userId}", httpMethod = HttpMethod.PUT)
	public void updateProfile(@Named("userId") String userId, final ProfileDTO pDto) throws UnauthorizedException{
		if(userId != null){
			ps.updateUserProfile(userId,pDto);
		}else{
			throw new UnauthorizedException("User is not authorized.");
		}
	}
}
