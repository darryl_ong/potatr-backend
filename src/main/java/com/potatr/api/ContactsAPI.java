package com.potatr.api;

import java.util.List;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.potatr.constants.AuthConstants;
import com.potatr.dto.ContactPermissionsDTO;
import com.potatr.dto.ProfileDTO;
import com.potatr.service.ContactService;
import com.potatr.service.ProfileService;

@Api(
	    name = "potatr",
	    version = "v1",
	    scopes = {AuthConstants.EMAIL_SCOPE},
	    clientIds = {AuthConstants.WEB_CLIENT_ID, AuthConstants.API_EXPLORER_CLIENT_ID}
	)
public class ContactsAPI {
	ContactService cs = new ContactService();
	ProfileService ps = new ProfileService();
	
	@ApiMethod(name = "contacts.list", path="contacts/list/{userId}", httpMethod = HttpMethod.GET)
	public List<ProfileDTO> listContacts(@Named("userId") String userId) throws UnauthorizedException{
		if(userId != null){
			return cs.getContacts(userId);
		}else{
			throw new UnauthorizedException("No authenticated user.");
		}
	}
	
	@ApiMethod(name = "contacts.put", path="contacts/put/{userId}", httpMethod = HttpMethod.PUT)
	public void putContact(@Named("userId") String userId, ContactPermissionsDTO cpd) throws UnauthorizedException{
		if(userId != null){
			cs.putContact(userId, cpd);
		}else{
			throw new UnauthorizedException("User is not authorized.");
		}
	}
}
