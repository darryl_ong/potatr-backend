package com.potatr.constants;

public class AuthConstants {
	 public static final String WEB_CLIENT_ID = "689259211100-s5g5djt01sa0amfms3me29s4ldvuhf8k.apps.googleusercontent.com";
	  public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
	  public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";
	  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

	  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
	  
	  public static final String API_EXPLORER_CLIENT_ID = com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID;
}
