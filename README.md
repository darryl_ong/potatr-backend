Potatr API
========

Base URL
--------------
https://potatr-1038.appspot.com/_ah/api/potatr/v1/

Profiles API
----------------
1. (GET) potatr.profile.get

  * https://potatr-1038.appspot.com/_ah/api/potatr/v1/profile/{userId}
  * returns the profile of user with userId
    ```
    {
     "email": "darryljohn.ong@gmail.com",
     "firstName": "Darryl",
     "lastName": "Ong",
     "position": "Software Engineer",
     "department": "Google",
     "mobileNumber": "09175119007",
     "officeNumber": "8220000",
     "officeAddress": "PhilamLife Tower, Paseo de Roxas, Makati"
    }
    ```

1. (PUT) potatr.profile.put

  * https://potatr-1038.appspot.com/_ah/api/potatr/v1/profile/{userId}
  * If profile with userId does not exist yet, creates a profile, otherwise it is updated

Contacts API
------------

1. (GET) potatr.contacts.list

  * https://potatr-1038.appspot.com/_ah/api/potatr/v1/contacts/list/{userId}
  * returns a list of user contacts in Profile format
    ```
    "items": [
     {
      "email": "russellraed@gmail.com",
      "firstName": "Russell",
      "lastName": "Gutierrez",
      "position": "Web Developer",
      "kind": "potatr#contactsItem"
     }
    ]
    ```

2. (PUT) potatr.contacts.put

  * https://potatr-1038.appspot.com/_ah/api/potatr/v1/contacts/put/{userId}
    ```
    Payload:
    {
      "contactId": "russellraed@gmail.com",
      "seeDepartment": "N",
      "seeFirstName": "Y",
      "seeLastName": "Y",
      "seeMobileNumber": "Y",
      "seeOfficeAddress": "N",
      "seeOfficeNumber": "N",
      "seePosition": "Y"
    }
    ```